const { merge } = require('lodash');
const covariable_resolver = require('./covariable_resolver');
const ocurrence_resolver = require('./ocurrence_resolver');
const disease_resolver  = require('./disease_resolver');

const resolvers = merge({
  Query: {
    echo: (parent, args) => `Your message is: ${args.message}`,
  }
},
  covariable_resolver.resolvers,
  ocurrence_resolver.resolvers,
  disease_resolver.resolvers);

module.exports = resolvers;
