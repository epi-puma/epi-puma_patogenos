async function ocurrence_by_id_covariable(id_covariable, filter, context) {
  const conn = await context.conn.psql;
  context.logger.log('Llamada a BD all_covariable');

  var filter_str = 'true'
  if(filter != '') {
    filter_str = filter
  }

  const query = `
    SELECT *
    FROM ocurrence
    WHERE id_covariable = ${id_covariable}
    	AND not gridid_state is null
      AND ${filter_str}
    `;

  console.log(query)

  const results = await conn.any(query);
  return results;
}

exports.ocurrence_by_id_covariable = ocurrence_by_id_covariable;

async function occurrences_by_taxon(query, context) {
  const conn = await context.conn.psql;
  context.logger.log('Llamada a BD occurrences_by_taxon');

  var filter_str = 'true'
  if(query != '') {
    filter_str = query
  }

  const query_filter = `
    SELECT *
    FROM ocurrence
    WHERE not gridid_state is null
      AND ${filter_str}
    `;

  console.log(query_filter)

  const results = await conn.any(query_filter);
  return results;
}

exports.occurrences_by_taxon = occurrences_by_taxon;