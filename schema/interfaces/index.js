const covariable_type = require('./covariable_type');
const ocurrence_type = require('./ocurrence_type');

module.exports = [
  covariable_type,
  ocurrence_type,
];