const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo_patogenos(message: String): String,
    all_patogenos_covariables(limit: Int!, filter: String!): [CovariablePatogenos!]!,
    ocurrence_patogenos_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrencePatogenos!]!,
    get_diseases_patogenos: [Disease]
    occurrences_by_taxon_patogenos(query: String!): [OcurrencePatogenos!]!
  
  }
`;

module.exports = schema;