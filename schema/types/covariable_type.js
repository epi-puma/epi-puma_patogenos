const { gql } = require('apollo-server');

module.exports = gql`
  type CovariablePatogenos implements Covariable @key(fields: "id") {
	id: ID!,
  	reino: String,
	phylum: String,
	clase: String,
	orden: String,
	familia: String,
	genero: String,
	epitetoespecifico: String,
	nombrecientifico: String,
	cells_state: [String!]!,
	cells_mun: [String!]!,
	cells_ageb: [String!]!
  }
`;
